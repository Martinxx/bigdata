import import_ipynb
from stocks import *
import findspark
from pyspark.sql import SparkSession
import sys

import warnings



def plotDF(graphic_mode, plot_df,xduration,ys, ylabel, title):
	if graphic_mode == False:
		return 
	fig, axis = plt.subplots()
	x = plot_df.select(collect_list('Date')).first()[0]
	for i in ys:
		y = plot_df.select(collect_list(i)).first()[0]
		axis.plot(x, y, label =i)

	plt.xlabel("Date by "+xduration)
	plt.ylabel(ylabel)
	plt.title(title)
	plt.xticks(rotation=70)
	plt.legend()
	plt.show()


def plotcandlestick(graphic_mode, plot_df, prefix='avg(', suffix=')', title=''):
	if graphic_mode == False:
		return 
	fig, axis = plt.subplots()
	plt.figure()
	width = 1
	width2 = 0.25
	plot_df = plot_df.select("*").withColumn("index", monotonically_increasing_id())
	#define up and down prices
	#plot_df = plot_df.withColumn("trday",monotonicallyIncreasingId)
	up = plot_df.where(plot_df[prefix+'Close'+suffix]>=plot_df[prefix+'Open'+suffix])
	down = plot_df.where(plot_df[prefix+'Close'+suffix]<plot_df[prefix+'Open'+suffix])
	#define colors to use
	col1 = 'green'
	col2 = 'red'
	up = up.withColumn('deltaOP', up[prefix+'Close'+suffix]-up[prefix+'Open'+suffix])
	up = up.withColumn('deltaHL', up[prefix+'High'+suffix]-up[prefix+'Low'+suffix])

	#plot up prices
	axis.bar(up.select(collect_list('index')).first()[0],\
		up.select(collect_list('deltaOP')).first()[0],\
		width,bottom=up.select(collect_list(prefix+'Open'+suffix)).first()[0],color=col1)
	axis.bar(up.select(collect_list('index')).first()[0],\
					up.select(collect_list('deltaHL')).first()[0],\
					width2,bottom=up.select(collect_list(prefix+'Low'+suffix)).first()[0],color=col1)


	down = down.withColumn('deltaOP', down[prefix+'Close'+suffix]-down[prefix+'Open'+suffix])
	down = down.withColumn('deltaHL', down[prefix+'High'+suffix]-down[prefix+'Low'+suffix])

	axis.bar(down.select(collect_list('index')).first()[0],\
		down.select(collect_list('deltaOP')).first()[0],\
		width,bottom=down.select(collect_list(prefix+'Open'+suffix)).first()[0],color=col2)
	axis.bar(down.select(collect_list('index')).first()[0],\
					down.select(collect_list('deltaHL')).first()[0],\
					width2,bottom=down.select(collect_list(prefix+'Low'+suffix)).first()[0],color=col2)

	#plot down prices
	'''plt.bar(down.index,down.close-down.open,width,bottom=down.open,color=col2)
				plt.bar(down.index,down.high-down.open,width2,bottom=down.open,color=col2)
				plt.bar(down.index,down.low-down.close,width2,bottom=down.close,color=col2)'''
	plt.title(title)
	plt.xticks(up.select(collect_list('Date')).first()[0],rotation='vertical')
	plt.subplots_adjust(bottom=0.3)
	plt.show()


def main():
	graphic_mode = False
	if len(sys.argv) > 1 and sys.argv[1] == '--graphic':
		graphic_mode = True
	findspark.init()
	spark = (SparkSession.builder\
		.master("local")\
		.appName("You_app_name")\
		.config('spark.ui.port', '4050')\
		.getOrCreate())

	path_data =  'stocks_data/'
	devColumns_double = StructType([StructField("Date",TimestampType()), 
               StructField("High",DoubleType()), 
               StructField("Low",DoubleType()), 
               StructField("Open",DoubleType()),
               StructField("Close",DoubleType()),
               StructField("Volume",DoubleType()),
               StructField("Adj Close",DoubleType()),
               StructField("company_name",StringType())])
	while 1:
		command = input('Enter what you want to compute [COMPANY]--[Year,Month,Day]--[Price,Average,Return,MA,BB,Correlation]\n')
		commands = command.split('--')
		if len(commands)<=2:
			continue
		else:
			df = addFuncs(extract_data('stocks_data/'+commands[0]+'.csv', devColumns_double, spark))

			if commands[2] == 'Price':
				#if commands[1] == days	
				plotcandlestick(graphic_mode, df, title =f'price({commands[1]})', prefix='', suffix = '')

			elif commands[2] == 'Average':
				if commands[1] != 'Day':
					columns = input('What column do you want (separate with \'--\')')

					plot_df = averagesByDuration(df, commands[1])
					plot_df.show()	
					plotDF(graphic_mode, plot_df,xduration=commands[1], ys=['avg('+x+')' for x in columns.split('--')] , ylabel='Averages',title =f'average on the periods ({commands[1]})')

				else:
					print('Day is not supported by average')

			elif commands[2] == 'Return':
				plot_df = getOpeningClosingByDuration(df, commands[1])
				plot_df.show()	
				plotDF(graphic_mode, plot_df,xduration=commands[1], ys=['Return'], ylabel='Return in %', title = f'{commands[0]} return by {commands[1]}')
				

			elif commands[2] == 'MA':
				values = input('What timespan do you want (separate with \'--\')').split('--')
				columns = input('What column do you want (separate with \'--\')').split('--')
				names,plot_df = updateAllMA(df,columns, values)
				plot_df.show()
				plotDF(graphic_mode, plot_df, xduration=commands[1], ys=names, ylabel='MAs', title = f'{commands[0]} MA by {commands[1]}')
			elif commands[2] == 'BB':
				plot_df = updateBB(df)
				plot_df.show()
				plotDF(graphic_mode, plot_df, xduration=commands[1], ys=names, ylabel='MAs', title = f'{commands[0]} MA by {commands[1]}')
			elif commands[2] == 'Correlation':
				print('xx')

warnings.filterwarnings("ignore", category=FutureWarning)
main()
'''
AMAZON--Month--Return
'''