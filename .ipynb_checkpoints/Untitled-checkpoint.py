#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import findspark
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *

path_data =  'stocks_data/'


# In[2]:


findspark.init()
spark = SparkSession.builder\
        .master("local")\
        .appName("You_app_name")\
        .config('spark.ui.port', '4050')\
        .getOrCreate()


# In[3]:


def read_disp_info(path, header=False, sch=None, delim=','):
    if sch:
        df = spark.read.schema(schema=sch).options(header=header, delimiter=delim).csv(path)
    else:
        df = spark.read.options(header=header, delimiter=delim).csv(path)
    return df

devColumns = ([StructField("Date",TimestampType()), 
               StructField("High",DoubleType()), 
               StructField("Low",DoubleType()), 
               StructField("Open",DoubleType()),
               StructField("Close",DoubleType()),
               StructField("Volume",LongType()),
               StructField("Adj Close",DoubleType()),
               StructField("company_name",StringType())])

amazon_df = read_disp_info(path_data + "/AMAZON.csv", True, StructType(devColumns))


#################Explore the different stocks historical prices#################
def showHeadTail(df):
    print(type(df))
    df.show(40)
    spark.createDataFrame(df.tail(40)).show(40)


# In[10]:



from pyspark.sql.window import Window

def observationTimeGap(df):
    return (df.withColumn('delta', 
            datediff(
                    df.Date,lag('Date', 1).over(
                        Window.partitionBy('company_name').orderBy('Date'))))
            .groupby('delta').count().sort(desc("count"))).collect()[0].delta

def getSummary(df):
    return df[['High','Low','Open','Close','Volume','Adj Close']].summary()

def showSummary(df):
    getSummary(df).show()

def getNa(df):
    print('Nan of null values depending on the numerical columns :')
    return df.select([count(when(isnan(c), c)).alias(c) for c in ['High','Low','Open','Close','Volume','Adj Close']])

def showNa(df):
    getNa(df).show()


amazon_df.show()

from pyspark.mllib.util import MLUtils
from pyspark.ml.feature import StandardScaler


'''
GALERE POUR NORMALISER LES DONNES
def getCorrMartix(df):
    for c in  ['High','Low','Open','Close','Volume','Adj Close']:
        scaler = StandardScaler(inputCol=c, outputCol=c,
                            withStd=True, withMean=False)
        scalerModel = scaler.fit(df)
        df = scalerModel.transform(df)
    return [[df.stat.corr(c, c1) for c in ['High','Low','Open','Close','Volume','Adj Close']] for c1 in ['High','Low','Open','Close','Volume','Adj Close']]

def showCorrMartix(df):
    mat = getCorrMartix(df)
    for i in mat:
        for j in i:
            print("%.2f" % j, end = ' | ')
        print()
showCorrMartix(amazon_df)
'''
delta_days = observationTimeGap(amazon_df)
#amazon_df = amazon_df.withColumn('xxx', functions.lag('Date', 1).over(Window.partitionBy('company_name').orderBy('Date')))


print(f'time in day between measures : {delta_days}')
showSummary(amazon_df)
showNa(amazon_df)

